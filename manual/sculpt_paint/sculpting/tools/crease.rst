
******
Crease
******

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Crease`

Create sharp indents or ridges by pushing or pulling the mesh, while pinching the vertices together.

Crease can also be used to sharpen and polish existing creases.
Enable pressure sensitivity on Strength to regulate the add/subtract effect while pinching creases.


Brush Settings
==============

General
-------

Pinch
   Adds a consistent pinching effect to your stroke.
   If set to 0 it the brush will behave like the
   :doc:`Draw </sculpt_paint/sculpting/tools/draw>` brush.
   If set to 1 and the brush strength set to 0, the brush
   will behave like a :doc:`Pinch </sculpt_paint/sculpting/tools/pinch>` brush.

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.
